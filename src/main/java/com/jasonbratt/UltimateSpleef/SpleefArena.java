package com.jasonbratt.UltimateSpleef;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;

import java.util.Random;



/**
 * Created by jasonbratt on 10/31/16.
 */
public class SpleefArena {
    final private Material defaultBlock = Material.SNOW_BLOCK;
    private Location corner1;
    private Location corner2;
    private ArrayList<BlockState> map;
    private HashMap<String, Player> players;

    public String name;


    public SpleefArena(String name) {
        this.name = name;
        this.players = new HashMap<String, Player>();
    }

    public void setCorner1(Location loc) {
        this.corner1 = loc;
    }

    public void setCorner2(Location loc) {
        this.corner2 = loc;
    }

    public void addPlayer(Player p) {
        if (!players.containsKey(p.getName())) {
            players.put(p.getName(), p);
            p.sendMessage("Joined arena " + this.name + " !");
        } else {
            p.sendMessage("Already in arena!");
        }
    }

    public void removePlayer(Player p) {
        if (players.containsKey(p.getName())) {
            players.remove(p.getName());
            p.sendMessage("Left arena!");
        }
    }

    public void startMatch() {
        this.resetMap();
        for (Player player : players.values()) {
            player.teleport(this.getRandomSpawn());
            player.sendMessage("Starting match bitches!");
        }
    }

    private Location getRandomSpawn() {
        Random rn = new Random();
        Location loc = map.get(rn.nextInt(map.size())).getLocation();
        loc.add(0, 1, 0);
        return loc;
    }


    public void endMatch() {

    }

    private void checkForLosers() {

    }

    public void saveMap(Player player) {
        if (this.corner1 != null && this.corner2 != null) {
            player.sendMessage(ChatColor.AQUA + "Saved map " + ChatColor.GREEN + this.name);
            this.createMap(player);
        } else {
            player.sendMessage(ChatColor.RED + "Not all of the corners have been set!");
        }
    }

    private void createMap(Player player) {
        map = new ArrayList<BlockState>();
        World world = player.getWorld();
        int largestX, largestY, largestZ, smallestX, smallestY, smallestZ;

        if (this.corner1.getBlockX() >= this.corner2.getBlockX()) {
            largestX = this.corner1.getBlockX();
            smallestX = this.corner2.getBlockX();
        } else {
            largestX = this.corner2.getBlockX();
            smallestX = this.corner1.getBlockX();
        }

        if (this.corner1.getBlockZ() >= this.corner2.getBlockZ()) {
            largestZ = this.corner1.getBlockZ();
            smallestZ = this.corner2.getBlockZ();
        } else {
            largestZ = this.corner2.getBlockZ();
            smallestZ = this.corner1.getBlockZ();
        }

        if (this.corner1.getBlockY() >= this.corner2.getBlockY()) {
            largestY = this.corner1.getBlockY();
            smallestY = this.corner2.getBlockY();
        } else {
            largestY = this.corner2.getBlockY();
            smallestY = this.corner1.getBlockY();
        }

        for (int x = smallestX; x <= largestX; x++) {
            for (int z = smallestZ; z <= largestZ; z++) {
                for (int y = smallestY; y <= largestY; y++) {
                    Block block = world.getBlockAt(x, y, z);
                    BlockState state = block.getState();
                    state.setType(this.defaultBlock);
                    map.add(state);
                }
            }
        }
    }

    private ArrayList<BlockState> getStates() {
        return map;
    }

    private void resetMap() {
        for (BlockState state : map) {
            state.update(true);
        }
    }

}
