package com.jasonbratt.UltimateSpleef;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by jasonbratt on 11/1/16.
 */
public class Spleef extends JavaPlugin implements CommandExecutor {
    private SpleefManager manager;
    @Override
    public void onEnable() {
        manager = new SpleefManager();
        this.getCommand("spleef").setExecutor(this);
    }

    public void onDisable() {

    }

    public String formatLocation(Location loc) {
        return "(" + loc.getBlockX() + ", " + loc.getBlockY() + ", " + loc.getBlockZ() + ")";
    }

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        boolean error = false;
        Player p = (Player) sender;

        if (args.length > 0) {
            if (args[0].equalsIgnoreCase("create")) {
                if (args.length > 1 && args[1].length() > 0) {
                    manager.addArena(args[1], p);
                } else {
                    error = true;
                }
            } else if (args[0].equalsIgnoreCase("remove")) {
                if (args.length > 1 && args[1].length() > 0) {
                    manager.removeArena(args[1], p);
                } else {
                    error = true;
                }
            } else if (args[0].equalsIgnoreCase("list")) {
                manager.listArenas((Player) sender);
            } else if (args[0].equalsIgnoreCase("arena")) {
                if (args.length > 1 && args[1].length() > 0) {
                    SpleefArena arena = manager.getArena(args[1]);
                    if (arena != null) {
                        if (args.length > 2 && args[2].length() > 0) {
                            Location pLoc = p.getLocation();
                            if (args[2].equalsIgnoreCase("c1")) {
                                arena.setCorner1(pLoc);
                                p.sendMessage(String.format(ChatColor.GREEN + arena.name + ": " + ChatColor.AQUA + "Set corner 1 %s", this.formatLocation(pLoc)));
                            } else if (args[2].equalsIgnoreCase("c2")) {
                                arena.setCorner2(pLoc);
                                p.sendMessage(String.format(ChatColor.GREEN + arena.name + ": " + ChatColor.AQUA + "Set corner 2 %s", this.formatLocation(pLoc)));
                            } else if (args[2].equalsIgnoreCase("save")) {
                                arena.saveMap(p);
                            } else if (args[2].equalsIgnoreCase("join")) {
                                arena.addPlayer(p);
                            } else if (args[2].equalsIgnoreCase("leave")) {
                                arena.removePlayer(p);
                            } else if (args[2].equalsIgnoreCase("start")) {
                                arena.startMatch();
                            }
                        }
                    } else {
                        error = true;
                    }
                }
            }

            if (error) {
                p.sendMessage(ChatColor.RED + "Incorrect command! Fuck you!");
            }
        } else {
            p.sendMessage(ChatColor.AQUA + "====== ( Spleef ) ======");
            p.sendMessage(ChatColor.GREEN + "/spleef create <name> - create a spleef arena.");
            p.sendMessage(ChatColor.GREEN + "/spleef remove <name> - remove a spleef arena");
            p.sendMessage(ChatColor.GREEN + "/spleef list - show all arenas.");
            p.sendMessage(ChatColor.GREEN + "/spleef arena <name> <c1|c2> - Set a corner for an arena.");
            p.sendMessage(ChatColor.GREEN + "/spleef arena <name> save - Save set corners for map.");
            p.sendMessage(ChatColor.GREEN + "/spleef arena <name> join - Join an arena.");
            p.sendMessage(ChatColor.GREEN + "/spleef arena <name> leave - Leave an arena.");
            p.sendMessage(ChatColor.GREEN + "/spleef arena <name> start - Start an arena.");

        }

        return !error;
    }
}
