package com.jasonbratt.UltimateSpleef;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by jasonbratt on 11/1/16.
 */
public class SpleefManager {
    private HashMap<String, SpleefArena> arenas;

    public SpleefManager() {
        arenas = new HashMap<String, SpleefArena>();
    }

    public void addArena(String arenaName, Player p) {
        if (arenaName.length() > 0) {
            arenas.put(arenaName, new SpleefArena(arenaName));
            p.sendMessage(String.format(ChatColor.AQUA + "Created new arena: " + ChatColor.GREEN + "%s", arenaName));
        }
    }

    public SpleefArena getArena(String arenaName) {
        if (arenas.containsKey(arenaName)) {
            return arenas.get(arenaName);
        }

        return null;
    }

    public void removeArena(String arenaName, Player p) {
        if (arenaName.length() > 0 && arenas.containsKey(arenaName)) {
            arenas.remove(arenaName);
            p.sendMessage(String.format(ChatColor.RED + "Removed arena: " + ChatColor.GREEN + "%s", arenaName));
        }
    }

    public void listArenas(Player p) {
        p.sendMessage(ChatColor.AQUA + "-- Arena List --");
        for (SpleefArena arena : arenas.values()) {
            p.sendMessage(String.format(ChatColor.GRAY + "%s", arena.name));
        }
    }
}
